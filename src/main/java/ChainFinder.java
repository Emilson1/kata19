import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Emil Michalczewski on 2017-05-07.
 */
public class ChainFinder {
    private final List<String> words;

    public ChainFinder(List<String> words) {
        this.words = words;
    }

    public List<String> getWordChain(String startingWord, String endingWord) {

        if (startingWord.length() != endingWord.length()) {
            return new ArrayList<>();
        }

        Map<String, String> path = new HashMap<>();

        Queue<String> vertexToVisit = new LinkedList<>();
        vertexToVisit.add(startingWord);

        //BFS

        String word = null;

        while (!vertexToVisit.isEmpty()) {
            word = vertexToVisit.poll();

            if (word.equals(endingWord)) {
                break;
            }

            List<String> nextWords = getNextWords(word);

            String visitedVertex = word;
            nextWords.stream()
                    .filter(w -> !path.containsKey(w))
                    .forEach(w -> {
                        path.put(w, visitedVertex);
                        vertexToVisit.add(w);

                    });
        }


        return getShortestPath(path, startingWord, endingWord);
    }

    private boolean isDifferByOneLetter(String word1, String word2) {
        if (word1.length() != word2.length()) {
            return false;
        }

        int diffs = 0;
        for (int n = 0; n < word1.length(); n++) {
            if (word1.charAt(n) != word2.charAt(n)) {
                diffs++;
            }
        }
        return diffs == 1;
    }


    private List<String> getNextWords(String word) {
        return words.stream()
                .filter(w -> isDifferByOneLetter(w, word))
                .collect(Collectors.toList());
    }

    private List<String> getShortestPath(Map<String, String> path, String startingWord, String endingWord) {
        if (path.isEmpty()) {
            return new ArrayList<>();
        }

        List<String> shortestPath = new ArrayList<>();
        String word = endingWord;
        while (!word.equals(startingWord)) {
            shortestPath.add(0, word);
            word = path.get(word);
        }

        shortestPath.add(0, startingWord);

        return shortestPath;
    }
}
