import java.util.List;

/**
 * Created by Emil Michalczewski on 2017-05-09.
 */
public class Main {
    public static void main(String[] args) {
        LetterLoader letterLoader = new LetterLoader("wordlist.txt");
        List<String> words = letterLoader.loadWords(5);

        ChainFinder chainFinder = new ChainFinder(words);

        List<String> wordChain = chainFinder.getWordChain("asdsa", "sdasd");
        if (wordChain.isEmpty()) {
            System.out.println("Cannot find path between given words !");
            System.exit(0);
        }

        wordChain.forEach(System.out::println);

    }
}
