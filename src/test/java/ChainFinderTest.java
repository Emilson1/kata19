import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by Emil Michalczewski on 2017-05-09.
 */
public class ChainFinderTest {

    @Test
    public void eachWordIsDifferByOneLetterTest() {
        //given
        LetterLoader letterLoader = new LetterLoader("wordlist.txt");
        List<String> words = letterLoader.loadWords(4);
        ChainFinder chainFinder = new ChainFinder(words);
        //when
        List<String> wordChain = chainFinder.getWordChain("ruby", "code");
        //then
        for (int i = 0; i < wordChain.size() - 1; i++) {
            assertTrue(isDifferByOneLetter(wordChain.get(i), wordChain.get(i + 1)));
        }
    }


    @Test
    public void isPathEqualInBothDirections() {
        //given
        LetterLoader letterLoader = new LetterLoader("wordlist.txt");
        List<String> words = letterLoader.loadWords(4);
        ChainFinder chainFinder = new ChainFinder(words);
        //when
        List<String> forwardWordChain = chainFinder.getWordChain("ruby", "code");
        List<String> backwardWordChain = chainFinder.getWordChain("code", "ruby");
        //then
        assertTrue(forwardWordChain.size() == backwardWordChain.size());
        for (int i = 0, j = forwardWordChain.size() - 1; j >= 0 && i < forwardWordChain.size(); j--, i++) {
            assertTrue(forwardWordChain.get(i) == backwardWordChain.get(j));
        }

    }


    private boolean isDifferByOneLetter(String word1, String word2) {
        if (word1.length() != word2.length()) {
            return false;
        }
        int diffs = 0;
        for (int n = 0; n < word1.length(); n++) {
            if (word1.charAt(n) != word2.charAt(n)) {
                diffs++;
            }
        }
        return diffs == 1;
    }
}
