import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Emil Michalczewski on 2017-05-07.
 */
public class LetterLoader {

    private final String pathToLetter;

    public LetterLoader(String pathToLetter) {
        this.pathToLetter = pathToLetter;
    }

    public List<String> loadWords(int wordLength) {
        List<String> words;
        try (Stream<String> lines = Files.lines(Paths.get(pathToLetter))) {

            words = lines
                    .filter(w -> w.length() == wordLength)
                    .collect(Collectors.toList());


        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while loading word letter !", e);
        }

        return words;
    }

}
