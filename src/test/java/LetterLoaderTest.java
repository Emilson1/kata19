import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.nio.file.NoSuchFileException;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by Emil Michalczewski on 2017-05-09.
 */
public class LetterLoaderTest {

    @Test
    public void shouldLoadWordsOfGivenLength() {
        //given
        LetterLoader letterLoader = new LetterLoader("wordlist.txt");
        int GIVEN_LENGTH = 3;
        //when
        List<String> words = letterLoader.loadWords(GIVEN_LENGTH);
        //then
        words.forEach(w -> assertTrue(w.length() == GIVEN_LENGTH));
    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowExceptionWhenFileNotFound() {
        //given
        String INVALID_PATH = "invalidpath.xyz";
        LetterLoader letterLoader = new LetterLoader(INVALID_PATH);
        try {
            //when
            letterLoader.loadWords(3);
            fail();
        } catch (Exception e) {
            //then
            assertTrue(e.getCause() instanceof NoSuchFileException);
            throw e;
        }

    }
}
